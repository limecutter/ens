// envoyer la commande 0xed (via l'input buffer)
// pour allumer les LED (qui doivent etre specifiees
// via l'input buffer)

    outb $0xed, $0x60

// attendre que l'input buffer soit vide (donnee
// d'une precedente commande)

boucle:
    inb  $0x64, %al   // AL = status register
    testb $0x02, %al  // teste le bit 1 (INPB) du registre AL
    jnz boucle	      // bit 1 != 0 : input buffer non vide

// specifier les LED a allumer via les bits dans l'input buffer
// 0x01 : ScrollLock, 0x02 : NumLock, 0x04 : CapsLock

    outb $0x06, $0x60 // ecrire dans l'input buffer
